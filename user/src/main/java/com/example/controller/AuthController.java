package com.example.controller;

import com.example.request.*;
import com.example.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {

    //3

    private final UserService userService;

    @PostMapping("/addAdmin")
    public SignUpResponseDTO signUpAdmin(@Valid @RequestBody Igronerequest.AddAdmin request){
        return userService.signUpAdmin(request);
    }

    @PostMapping("/addDoctor")
    public SignUpResponseDTO signUpDoctor(@Valid @RequestBody Igronerequest.AddAdmin request){
        return userService.signUpDoctor(request);
    }
    @PostMapping("/signin")
    public LoginResponseDTO login(@Valid @RequestBody LoginRequestDTO loginRequestDTO) {
        return userService.login(loginRequestDTO);
    }


}
