package com.example.mapper;

import com.example.entity.Users;
import com.example.request.LoginResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LoginMapper extends Base<Users, LoginResponseDTO> {

    LoginMapper INSTANCE = Mappers.getMapper(LoginMapper.class);

    LoginResponseDTO toDTO(Users x);
}
