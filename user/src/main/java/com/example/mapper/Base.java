package com.example.mapper;

import com.example.request.SignUpRequestDTO;

import java.util.List;

public interface Base<D, V> {

    D toEntity(V v);
    V toDTO(D d);

    List<D> toEntities(List<V> v);
//    List<V> toDTOList(List<D> d);
}
