package com.example.mapper;

import com.example.entity.Users;
import com.example.request.LoginResponseDTO;
import com.example.request.SignUpRequestDTO;
import com.example.request.SignUpResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SignUpMapper {

    SignUpMapper INSTANCE = Mappers.getMapper(SignUpMapper.class);

    SignUpResponseDTO toResponse(Users d);

    Users toEntity(SignUpRequestDTO x);
}
