package com.example.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class LoginResponseDTO extends BaseResponseDTO {

    private String userId;

    private String fullName;

}
