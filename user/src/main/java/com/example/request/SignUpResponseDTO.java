package com.example.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SignUpResponseDTO extends BaseResponseDTO{
    private String fullName;
    private String userName;
}
