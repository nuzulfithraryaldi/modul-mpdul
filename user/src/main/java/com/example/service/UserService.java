package com.example.service;

import com.example.request.LoginRequestDTO;
import com.example.request.LoginResponseDTO;
import com.example.request.SignUpRequestDTO;
import com.example.request.SignUpResponseDTO;

public interface UserService {
    LoginResponseDTO login (LoginRequestDTO loginRequest);

    SignUpResponseDTO signUpDoctor(SignUpRequestDTO signUpRequestDTO);

    SignUpResponseDTO signUpAdmin(SignUpRequestDTO signUpRequestDTO);


}
