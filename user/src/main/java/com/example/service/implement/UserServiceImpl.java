package com.example.service.implement;


import com.example.UserEnum;
import com.example.entity.Users;
import com.example.mapper.LoginMapper;
import com.example.mapper.SignUpMapper;
import com.example.repository.UserRepository;
import com.example.request.LoginRequestDTO;
import com.example.request.LoginResponseDTO;
import com.example.request.SignUpRequestDTO;
import com.example.request.SignUpResponseDTO;
import com.example.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public LoginResponseDTO login(LoginRequestDTO loginRequest){
        try {
            Users user = userRepository.findByUserName(loginRequest.getUserName()).orElse(null);
            LoginResponseDTO responseDTO = LoginMapper.INSTANCE.toDTO(user);
            if (user == null) {
                return responseDTO;
            }
            if (!loginRequest.getPassword().equals(user.getPassword())) {
                return responseDTO;
            }
            return responseDTO;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public SignUpResponseDTO signUpAdmin(SignUpRequestDTO request) {
        try {
            Users entity = SignUpMapper.INSTANCE.toEntity(request);
            request.setRole(UserEnum.USER_TYPE_ADMIN.name());
            if (request.getRole() != null && request.getRole() != "") {
                entity.setRole(request.getRole());
            }
            Users result = userRepository.save(entity);
            return SignUpMapper.INSTANCE.toResponse(result);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public SignUpResponseDTO signUpDoctor(SignUpRequestDTO request) {
        try {
            Users entity = SignUpMapper.INSTANCE.toEntity(request);
            request.setRole(UserEnum.USER_TYPE_DOCTOR.name());
            if (request.getRole() != null && request.getRole() != "") {
                entity.setRole(request.getRole());
            }
            Users result = userRepository.save(entity);
            return SignUpMapper.INSTANCE.toResponse(result);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}

