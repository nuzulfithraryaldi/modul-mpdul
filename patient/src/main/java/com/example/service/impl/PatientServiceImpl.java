package com.example.service.impl;

import com.example.entity.Patients;
import com.example.mapper.CreatePatientMapper;
import com.example.repository.PatientRepository;
import com.example.repository.UserRepository;
import com.example.request.PatientCreateResponseDTO;
import com.example.request.PatientRequestDTO;
import com.example.service.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PatientServiceImpl implements PatientService {

    private final PatientRepository patientRepository;

    private final UserRepository userRepository;


    @Override
    public PatientCreateResponseDTO createPatient(PatientRequestDTO request) {
        Patients entity = CreatePatientMapper.INSTANCE.toEntity(request);
        entity.setUsers(userRepository.findById(request.getUserId()).orElse(null));
        Patients result = patientRepository.save(entity);
        return CreatePatientMapper.INSTANCE.toResponse(result);
    }

    @Override
    public ResponseEntity<?> editPatient(PatientRequestDTO patientRequest) {
        return null;
    }

    @Override
    public ResponseEntity<Object> deletePatientById(Long patientId) {
        return null;
    }

    @Override
    public PatientCreateResponseDTO getPatientById(Long patientId) {
        try {
            Patients patients = patientRepository.findById(patientId).orElse(null);
            return CreatePatientMapper.INSTANCE.toResponse(patients);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
