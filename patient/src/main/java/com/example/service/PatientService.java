package com.example.service;

import com.example.request.PatientCreateResponseDTO;
import com.example.request.PatientRequestDTO;
import org.springframework.http.ResponseEntity;

public interface PatientService {


    PatientCreateResponseDTO createPatient(PatientRequestDTO request);

    ResponseEntity<?> editPatient(PatientRequestDTO patientRequest);

    ResponseEntity<Object> deletePatientById(Long patientId);



    PatientCreateResponseDTO getPatientById (Long patientId);
}
