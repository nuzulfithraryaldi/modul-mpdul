package com.example.controller;

import com.example.request.PatientCreateResponseDTO;
import com.example.request.PatientRequestDTO;
import com.example.service.PatientService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/patient")
public class PatientController {

    private final PatientService patientService;

    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @PostMapping("/addPatient")
    public PatientCreateResponseDTO create(@Valid @RequestBody PatientRequestDTO request) {
        return patientService.createPatient(request);
    }

    @GetMapping("/getPatientById/{patientId}")
    public PatientCreateResponseDTO getPatientById(@PathVariable Long patientId) {
        return patientService.getPatientById(patientId);
    }
}
