package com.example.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PatientCreateResponseDTO extends BaseResponseDTO{
    private String patientName;
    private String complaints;
}
