package com.example.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public abstract class BaseResponseDTO implements Serializable {

    @JsonProperty("id")
    protected String userId;

//    @Override
//    public int hashCode() {
//        return userId.hashCode();
//    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        BaseResponseDTO that = (BaseResponseDTO) obj;
        return Objects.equals(userId, that.userId);
    }
}