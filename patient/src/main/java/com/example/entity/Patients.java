package com.example.entity;

import com.sun.javafx.beans.IDProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;


@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="patient_table")
public class Patients {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long patientId;

    private String patientName;

    private String birthPlace;

    private String birthDate;

    private String address;

    private String gender;

    private String complaints;

    private Instant registrationDate;

    @OneToMany(targetEntity = Treatments.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "patientsId", referencedColumnName = "patientId")
    private List<Treatments> treatments;

    @ManyToOne
    @JoinColumn(name = "userId")
    private Users users;

}
