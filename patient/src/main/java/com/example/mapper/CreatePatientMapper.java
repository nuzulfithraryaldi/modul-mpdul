package com.example.mapper;

import com.example.entity.Patients;
import com.example.request.PatientCreateResponseDTO;
import com.example.request.PatientRequestDTO;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.http.ResponseEntity;

public interface CreatePatientMapper {
    CreatePatientMapper INSTANCE = Mappers.getMapper(CreatePatientMapper.class);

    Patients toEntity(PatientRequestDTO v);

    @Mapping(source = "users.userId",target = "userId")
    PatientRequestDTO toDTO(Patients x);

    PatientCreateResponseDTO toResponse(Patients result);
}
