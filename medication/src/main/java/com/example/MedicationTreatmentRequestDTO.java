package com.example;

import lombok.Data;

@Data
public class MedicationTreatmentRequestDTO {
    private Long medicationId;
}
