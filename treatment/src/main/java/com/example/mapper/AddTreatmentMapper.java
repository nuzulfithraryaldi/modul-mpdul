package com.example.mapper;

import com.example.entity.Treatments;
import com.example.request.TreatmentRequestDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AddTreatmentMapper {

    AddTreatmentMapper INSTANCE = Mappers.getMapper(AddTreatmentMapper.class);

    Treatments toEntity(TreatmentRequestDTO v);

    TreatmentRequestDTO toDTO(Treatments x);
}
