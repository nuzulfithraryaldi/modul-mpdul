package com.example.request;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.time.Instant;
import java.util.List;

@Data
@RequiredArgsConstructor
public class TreatmentRequestDTO {
    private Long treatmentId;

    private Long patientId;

    private String sickness;

    private String sicknessDesc;

    private String sicknessHandling;

    private Instant createTime;

//    private List<MedicationTreatmentRequestDTO> medications;
}
