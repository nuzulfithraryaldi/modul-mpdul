package com.example.service.implement;

import com.example.entity.Patients;
import com.example.entity.Treatments;
import com.example.mapper.AddTreatmentMapper;
import com.example.repository.PatientRepository;
import com.example.repository.TreatmentRepository;
import com.example.request.TreatmentRequestDTO;
import com.example.service.TreatmentService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class TreatmentServiceImpl implements TreatmentService {

    private final PatientRepository patRepository;
    private final TreatmentRepository treatmentRepository;

    public TreatmentServiceImpl(PatientRepository patRepository, TreatmentRepository treatmentRepository) {
        this.patRepository = patRepository;
        this.treatmentRepository = treatmentRepository;
    }

    @Override
    public ResponseEntity<Object> addTreatment(TreatmentRequestDTO request) {
        try {
            Treatments entity = AddTreatmentMapper.INSTANCE.toEntity(request);
            entity.getPatients(patRepository.findById(request.getPatientId()).orElse(null));
            Treatments result = treatmentRepository.save(entity);
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
