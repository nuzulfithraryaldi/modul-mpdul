package com.example.service;

import com.example.request.TreatmentRequestDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


public interface TreatmentService {
    ResponseEntity<Object> addTreatment(TreatmentRequestDTO treatmentRequest);
}
