package com.example.repository;

import com.example.entity.Treatments;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TreatmentRepository extends JpaRepository <Treatments,Long>{
}
